package API;

public class Pila<T> implements IStack<T> {

	/**
	 * Estructura de datos implementada en la lista
	 */
	private ListaEncadenada<T> list;
	
	/**
	 * Constructor de la pila encargado de crear una nueva lista vac�a
	 */
	public Pila()
	{
		list = new ListaEncadenada<T>();
	}
	
	/**
	 * Agrega un elemento al final de la pila
	 */
	@Override
	public void push(T item) {
		// TODO Auto-generated method stub
		list.agregarElementoFinal(item);
	}

	/**
	 * Elimina el �ltimo elemento de la pila
	 */
	@Override
	public T pop() {
		// TODO Auto-generated method stub
		return list.eliminarObjeto(list.darNumeroElementos()-1);
	}

	/**
	 * Indica si la pila est� vac�a o no
	 * @return true si lo est�
	 * @return false si no
	 */
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(list.darNumeroElementos() == 0)
			return true;
		
		else
			return false;
	}

	/**
	 * Devuelve el n�mero de elementos de la pila
	 */
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return list.darNumeroElementos();
	}

}
