package API;

public interface IStack<T> {

	/**
	 * Agrega un item al tope de la pila
	 * @param item - Item que se agregar� a la pila
	 */
	public void push(T item);
	
	/**
	 * Elimina el elemento en el tope de la pila
	 */
	public T pop();
	
	/**
	 * Indica si la pila est� vac�a
	 * @return true si est� vac�a
	 * @return false si no
	 */
	public boolean isEmpty();
	
	/**
	 * Da n�mero de elementos en la pila
	 * @return n�mero de elementos de la pila
	 */
	public int size();
}
