package API;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.Iterator;

import model.data_structures.*;
import vos.*;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas {

	private ListaEncadenada<VOTag> tagsList;

	private RedBlackBST<Integer,VOUsuario> userList;

	private ListaEncadenada<VORating> ratingList;

	private RedBlackBST<Integer, VOPelicula> peliculasId; //llave con id de la pelicula

	//llave con a�o de la pelicula
	private RedBlackBST<Integer, TablaHash1nodo<String, RedBlackBST<Double, ListaEncadenada<VOpeliculasUsuarios>>>> principal;

	private RedBlackBST<Integer, VOUsuario> usuariosId; //llave con id del usuario

	private RedBlackBST<Integer, VORelacionUsuarioPelicula> usuarioPeliculaList; // llave id usuarios

	private RedBlackBST<Integer, ListaEncadenada<VOSimilitud>> arbolSimilitud; // llave idPelicula

	private MaxHeapCP<VOUsuario> prioridad;

	private Cola<VOUsuario> bajaPrioridad;

	private TablaHash1nodo<String, RedBlackBST<Date, VOPelicula>> peliculasGenero; //llave genero

	private ListaEncadenada<VOClasificacion> diccionario;
	
	private RedBlackBST<Integer, VOUsuario> usuariosConformes; //llave idUsuario

	private RedBlackBST<Integer, VOUsuario> usuariosInconformes; //llave idUsuario
	
	private RedBlackBST<Integer, VOUsuario> usuariosNeutrales; // llave idUsuario
	
	private RedBlackBST<Integer, VOUsuario> usuariosNoClasificados; //llave idUsuario

	public SistemaRecomendacionPeliculas()
	{
		tagsList = new ListaEncadenada<VOTag>();
		userList = new RedBlackBST<>();
		ratingList = new ListaEncadenada<VORating>();
		principal = new RedBlackBST<>();
		peliculasId = new RedBlackBST<>();
		usuariosId = new RedBlackBST<>();
		arbolSimilitud = new RedBlackBST<>();
		usuarioPeliculaList = new RedBlackBST<>();
		prioridad = new MaxHeapCP<>();
		bajaPrioridad = new Cola<>();
		peliculasGenero = new TablaHash1nodo<>();
		diccionario = new ListaEncadenada<>();
		usuariosConformes = new RedBlackBST<>();
		usuariosInconformes = new RedBlackBST<>();
		usuariosNeutrales = new RedBlackBST<>();
		usuariosNoClasificados = new RedBlackBST<>();
	}

	@Override
	public ISistemaRecomendacionPeliculas crearSR() {
		// TODO Auto-generated method stub
		return new SistemaRecomendacionPeliculas();
	}

	public void calcularSimilitudesPeliculas()
	{
		ArrayList<VOPelicula> peliculasNeto = peliculasId.inOrden();
		for (int i = 0; i < peliculasNeto.size(); i++) {
			VOPelicula peliculaRef = peliculasNeto.get(i);
			for (int j = 0; j < peliculasNeto.size(); j++) {
				ArrayList<VORelacionUsuarioPelicula> arregloUsuariosPelicua = new ArrayList<VORelacionUsuarioPelicula>();
				VOPelicula peliculaAComparar = peliculasNeto.get(j);
				if(peliculaRef!= peliculaAComparar)
				{
					ListaEncadenada<VORelacionUsuarioPelicula> usuariosPeliculaRef = peliculaRef.getUsuariosConRating();
					ListaEncadenada<VORelacionUsuarioPelicula> usuariosPeliculaAComparar = peliculaAComparar.getUsuariosConRating();
					for (VORelacionUsuarioPelicula object : usuariosPeliculaRef) {
						VORelacionUsuarioPelicula ref = object;
						for (VORelacionUsuarioPelicula aComparar : usuariosPeliculaAComparar) {
							if(ref.getIdUsuario()== aComparar.getIdUsuario())
							{
								VORelacionUsuarioPelicula nuevo = new VORelacionUsuarioPelicula( ref.getIdUsuario(), ref.getRating()*aComparar.getRating());
								nuevo.setRatingPeliculaActual(ref.getRating());
								nuevo.setRatingPeliculaAComparar(aComparar.getRating());
								arregloUsuariosPelicua.add(nuevo);
							}
						}
					}
				}
				if(arregloUsuariosPelicua.size()>= 3 && j==peliculasNeto.size()-1)
				{

					double numerador =0;
					double denominador1 =0.0;
					double denominador2 =0.0;
					for (int k = 0; k < arregloUsuariosPelicua.size(); k++) {
						numerador += arregloUsuariosPelicua.get(i).getRating();
						denominador1 += (arregloUsuariosPelicua.get(i).getRatingPeliculaActual())*(arregloUsuariosPelicua.get(i).getRatingPeliculaActual());
						denominador2 += (arregloUsuariosPelicua.get(i).getRatingPeliculaAComparar())*(arregloUsuariosPelicua.get(i).getRatingPeliculaAComparar());
					}
					double similitud = numerador/((Math.sqrt(denominador1))*(Math.sqrt(denominador2)));

					VOSimilitud temp = new VOSimilitud(peliculaRef.getMovieID(), peliculaRef.getNombre(), peliculaAComparar.getMovieID(), peliculaAComparar.getNombre(), similitud);
					if(arbolSimilitud.contains(temp.getIdPeliculaActual()))
					{
						arbolSimilitud.get(temp.getIdPeliculaActual()).agregarElementoFinal(temp);;
					}
					else
					{
						ListaEncadenada<VOSimilitud> nuevaLista = new ListaEncadenada<>();
						nuevaLista.agregarElementoFinal(temp);
						arbolSimilitud.put(temp.getIdPeliculaActual(), nuevaLista);
					}
				}
			}
		}
	}

	public double calcularPrediccion(int idUsuario, int idPelicula)
	{
		double sumatoria1 = 0.0;
		double sumatoria2 = 0.0;
		VOUsuario usuario = userList.get(idUsuario);
		ListaEncadenada<VOPelicula> peliculasUsuarioHaCalificado = new ListaEncadenada<>();
		if(!usuario.getRatings().isEmpty())
		{
			for(VORating rating : usuario.getRatings())
			{
				if(peliculasId.contains(rating.getMovieID()))
					peliculasUsuarioHaCalificado.agregarElementoFinal(peliculasId.get(rating.getMovieID()));
			}
		}
		for(int i = 0; i < peliculasUsuarioHaCalificado.darNumeroElementos(); i++)
		{
			VOPelicula peliculaCalificada = peliculasUsuarioHaCalificado.darElemento(i);
			double ratingPeliculaCalificada = darRatingUsuarioPelicula(usuario.getIdUsuario(), peliculaCalificada.getMovieID());
			double similitudPeliculas = 0.0; 
			
			ListaEncadenada<VOSimilitud> similitudesPeliculaCalificada = arbolSimilitud.get(peliculaCalificada.getMovieID());
			for(VOSimilitud similitud : similitudesPeliculaCalificada)
			{
				if(similitud.getIdPeliculaActual() == peliculaCalificada.getMovieID() && similitud.getIdPeliculaComparada() == idPelicula)
				{
					similitudPeliculas = similitud.getSimilitud();
				}
			}
			
			sumatoria1 += similitudPeliculas * ratingPeliculaCalificada;
			sumatoria2 += similitudPeliculas;
		}

		

		return sumatoria1/sumatoria2;
	}

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) throws Exception {

		boolean respuesta = false;
		try {
			BufferedReader buf = new BufferedReader(new FileReader(rutaPeliculas));
			String archivo = buf.readLine();
			JsonParser json_parser = new JsonParser();
			JsonArray array = json_parser.parse(archivo).getAsJsonArray();

			for (int i = 0; i < array.size(); i++) {				
				//construccion de una pelicula
				JsonObject actual = (JsonObject)array.get(i);
				JsonObject imdbData = actual.get("imdbData").getAsJsonObject();
				//atributos
				String nombre = imdbData.get("Title").getAsString();
				String date = imdbData.get("Released").getAsString();
				int a�o = 0;
				String mes ="";
				int dia=0;
				if(date == null || date.equals(""))
				{
					a�o = Integer.parseInt(imdbData.get("Year").getAsString());	
					if(imdbData.get("Year").getAsString() == null || imdbData.get("Year").getAsString().equals(""))
						a�o = 0;
				}
				else
				{
					String[] dates = date.split(" ");
					if(dates.length==3)
					{
						try {
							dia = Integer.parseInt(dates[0]);
						} catch (Exception e) {
						}
						mes = dates[1];
						a�o = Integer.parseInt(dates[2]);
					}
				}
				String[] generos = imdbData.get("Genre").getAsString().split(",");
				ListaEncadenada<VOGeneroPelicula> listaGeneros = new ListaEncadenada<>();
				for (int j = 0; j < generos.length; j++) {
					listaGeneros.agregarElementoFinal(new VOGeneroPelicula(generos[j]));
				}
				int votosTotales=0;
				try {
					String vot = imdbData.get("imdbVotes").getAsString().replace(',', '\0');
					votosTotales = Integer.parseInt(vot);
				} catch (Exception e) {
				}

				int promedioVotos = (int) (votosTotales/(a�o-2017));
				int movieId = actual.get("movieId").getAsInt();
				double ratingIMBD =0.0;
				try {
					ratingIMBD = imdbData.get("imdbRating").getAsDouble();
				} catch (Exception e) {
				}
				ArrayList<String> pais = new ArrayList<>();
				try {
					String paisaux = imdbData.get("Country").getAsString();
					String[] aux2 = paisaux.split(",");
					{
						for (int j = 0; j < aux2.length; j++) {
							pais.add(aux2[i]);
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				VOPelicula nuevo = new VOPelicula(nombre, a�o,mes,dia,listaGeneros, votosTotales, promedioVotos, ratingIMBD, movieId,pais);

				System.out.println("."+ nuevo.getA�o() +","+ nuevo.getGenerosAsociados().darElementoPosicionActual().getNombre() +","+ nuevo.getMovieID()+","+ nuevo.getNombre());
				peliculasId.put(movieId, nuevo);
			}

			respuesta = true;
		} catch (Exception e) {
			throw e;
		}
		return respuesta;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRaitings) {
		// TODO Auto-generated method stub
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaRaitings)));

			String linea = br.readLine();
			linea = br.readLine();

			while(linea != null)
			{
				String[] info = linea.split(",");

				VORating vor = new VORating();

				vor.setUserID(Integer.parseInt(info[0]));
				vor.setMovieID(Integer.parseInt(info[1]));
				vor.setRating(Double.valueOf(info[2]));
				vor.setTimestamp(Long.parseLong(info[3]));
				vor.setError(Double.valueOf(info[4]));

				ratingList.agregarElementoFinal(vor);

				VORelacionUsuarioPelicula voup = new VORelacionUsuarioPelicula(vor.getUserID(), vor.getRating());
				usuarioPeliculaList.put(voup.getIdUsuario(), voup);

				linea = br.readLine();
			}
			br.close();

			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		// TODO Auto-generated method stub
		try
		{
			File archivo = new File(rutaTags);
			FileReader fr = new FileReader(archivo);
			BufferedReader br = new BufferedReader(fr);

			String linea = br.readLine();
			linea = br.readLine();
			while(linea != null)
			{
				VOTag vot = new VOTag();
				vot.setContenido(linea);

				tagsList.agregarElementoFinal(vot);

				linea = br.readLine();
			}
			br.close();
			fr.close();
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}

	@Override
	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		return peliculasId.size();
	}

	@Override
	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		return userList.size();
	}

	@Override
	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		return tagsList.darNumeroElementos();
	}

	@SuppressWarnings({ "unused", "resource" })
	@Override
	public void registrarSolicitudRecomendacion(Integer idUsuario, String ruta) throws Exception {
		//caso prioridad alta
		if(idUsuario != null && ruta ==null)
		{
			VOUsuario actual = usuariosId.get(idUsuario);
			prioridad.agregar(actual, new Comparator<VOUsuario>() {

				@Override
				public int compare(VOUsuario arg1, VOUsuario arg2) {
					//busco la fecha mas baja entre los rating y tag
					long fechaArg1 =0;
					ListaEncadenada<VORating> arg1lista1 =arg1.getRatings();
					ListaEncadenada<VOTag> arg1lista2 =arg1.getTags();
					for (VORating voRating : arg1lista1) {
						if(voRating.getTimestamp()<fechaArg1)
						{
							fechaArg1 = voRating.getTimestamp();
						}
					}
					for (VOTag voTag : arg1lista2) {

						long timestampA = voTag.getTimestamp();
						if(timestampA<fechaArg1)
						{
							fechaArg1 = timestampA;
						}
					}
					long fechaArg2 =0;
					ListaEncadenada<VORating> arg2lista1 =arg1.getRatings();
					ListaEncadenada<VOTag> arg2lista2 =arg1.getTags();
					for (VORating voRating : arg2lista1) {
						if(voRating.getTimestamp()<fechaArg1)
						{
							fechaArg2 = voRating.getTimestamp();
						}
					}
					for (VOTag voTag : arg2lista2) {
						long timestampA = voTag.getTimestamp();
						if(timestampA<fechaArg1)
						{
							fechaArg2 = timestampA;
						}
					}
					Date dateArg1 = new Date(fechaArg1);
					Date dateArg2 = new Date(fechaArg2);
					//empiezo a comparar
					if(dateArg1.compareTo(dateArg2)==0)
					{
						int x = arg1.getRatings().darNumeroElementos();
						int y = arg2.getRatings().darNumeroElementos();
						if(x>y)
						{
							return 1;
						}
						else if(x<y)
						{
							return -1;
						}
						else
						{
							int w = arg1.getTags().darNumeroElementos();
							int z = arg2.getTags().darNumeroElementos();
							if(w>z)
							{
								return 1;
							}
							else if(w<z)
							{
								return -1;
							}
							else
							{
								return 0;
							}
						}
					}
					else
					{
						return dateArg1.compareTo(dateArg2);
					}
				}
			});

		}
		//caso baja prioridad
		else if(idUsuario == null && ruta !=null)
		{
			try {

				//creo los atributos del futuro objeto VOUsuario
				String userName = "";
				ListaEncadenada<VORating> listaRating = new ListaEncadenada<>();

				//empiezo a leer el json
				BufferedReader buf = new BufferedReader(new FileReader(ruta));
				String archivo = buf.readLine();
				JsonParser json_parser = new JsonParser();
				JsonObject objeto = json_parser.parse(archivo).getAsJsonObject();
				JsonObject request = objeto.get("request").getAsJsonObject();
				//le doy valores a name
				userName = request.get("user_name").getAsString();

				JsonArray array = objeto.get("ratings").getAsJsonArray();
				for (JsonElement jsonElement : array) {
					VORating nuevorating = new VORating();
					JsonObject objetoActual = (JsonObject) jsonElement;
					nuevorating.setMovieID(objetoActual.get("item_id").getAsInt());
					nuevorating.setRating(objetoActual.get("rating").getAsDouble());
					listaRating.agregarElementoFinal(nuevorating);
				}
				//creo un nuevo VOUsuario para encolarlo
				VOUsuario nuevoUser = new VOUsuario();

				nuevoUser.setRatings(listaRating);
				nuevoUser.setNombre(userName);
				bajaPrioridad.enqueue(nuevoUser);				

			} catch (Exception e) {
				// TODO: handle exception
			}

		}

	}

	@Override
	public void generarRespuestasRecomendaciones() {
		// TODO Auto-generated method stub

		// Crea la fecha y la hora a la que se gener� el archivo
		Calendar c1 = Calendar.getInstance();
		String dia, mes, a�o, hora, min, seg;
		dia = Integer.toString(c1.get(Calendar.DATE));
		mes = Integer.toString(c1.get(Calendar.MONTH));
		a�o = Integer.toString(c1.get(Calendar.YEAR));
		hora = Integer.toString(c1.get(Calendar.HOUR));
		min = Integer.toString(c1.get(Calendar.MINUTE));
		seg = Integer.toString(c1.get(Calendar.SECOND));
		String ruta = "./data/response/Recomendaciones_<"+dia+"-"+mes+"-"+a�o+"_"+hora+"_"+min+"_"+seg+">.json";
		File archivo = new File(ruta);
		try
		{
			//Genera el archivo
			PrintWriter pw = new PrintWriter(archivo);
			ListaEncadenada<VOUsuario> usuariosAMostrarLista = new ListaEncadenada<>();
			int cantidadUsuariosAMostrar = 10 - usuariosAMostrarLista.darNumeroElementos();
			Gson generador = new GsonBuilder().setPrettyPrinting().create();
			ListaEncadenada<ListaEncadenada<VOUsuarioGenerador>> response = new ListaEncadenada<>();
			ListaEncadenada<VOUsuarioGenerador> response_users = new ListaEncadenada<>();
			//Caso de usuarios alta prioridad
			if(!prioridad.esVacia() && !(cantidadUsuariosAMostrar==0))
			{
				//Recorrer la lista de usuarios de alta prioridad, agregarlos a una lista
				//para as� obtener los 10 con mayor prioridad e imprimir sus datos en el .json
				for(int i = 0; i < prioridad.darNumElementos() && i <= cantidadUsuariosAMostrar; i++)
				{
					usuariosAMostrarLista.agregarElementoFinal(prioridad.darElemento(i));
				}
				for(int i = 0; i < usuariosAMostrarLista.darNumeroElementos(); i++)
				{
					VOUsuario vou = usuariosAMostrarLista.darElemento(i);
					VOUsuarioGenerador userGen = new VOUsuarioGenerador();
					ListaEncadenada<VORatingGenerador> recommendations = new ListaEncadenada<>();

					userGen.setUserId(vou.getIdUsuario());

					ListaEncadenada<VORating> ratings = vou.getRatings();
					for(int j = 0; j < ratings.darNumeroElementos(); j++)
					{
						VORating vor = ratings.darElemento(j);
						VORatingGenerador vorg = new VORatingGenerador();
						vorg.setItemId(vor.getMovieID());
						vorg.setPRating(vor.getRating());
						recommendations.agregarElementoFinal(vorg);
					}
					userGen.setRecommendations(recommendations);
					response_users.agregarElementoFinal(userGen);
				}
				response.agregarElementoFinal(response_users);
			}
			//Caso usuarios baja prioridad
			else
			{
				if(!bajaPrioridad.isEmpty() && !(cantidadUsuariosAMostrar==0))
				{
					for(int i = 0; i < bajaPrioridad.size() && i < cantidadUsuariosAMostrar; i++)
					{
						usuariosAMostrarLista.agregarElementoFinal(bajaPrioridad.dequeue());
					}
					for(int i = 0; i < usuariosAMostrarLista.darNumeroElementos(); i++)
					{
						VOUsuario vou = usuariosAMostrarLista.darElemento(i);
						VOUsuarioGenerador userGen = new VOUsuarioGenerador();
						ListaEncadenada<VORatingGenerador> recommendations = new ListaEncadenada<>();

						userGen.setUserId(vou.getIdUsuario());

						ListaEncadenada<VORating> ratings = vou.getRatings();
						for(int j = 0; j < ratings.darNumeroElementos(); j++)
						{
							VORating vor = ratings.darElemento(j);
							VORatingGenerador vorg = new VORatingGenerador();
							vorg.setItemId(vor.getMovieID());
							vorg.setPRating(vor.getRating());
							recommendations.agregarElementoFinal(vorg);
						}
						userGen.setRecommendations(recommendations);
						response_users.agregarElementoFinal(userGen);
					}
					response.agregarElementoFinal(response_users);
				}
				else
				{
					pw.close();
					return;
				}
			}
			pw.println(generador.toJson(response));
			pw.close();
		}
		catch(IOException ioe)
		{
			return;
		}
	}

	@Override
	public ListaEncadenada<VOPelicula> peliculasGenero(VOGeneroPelicula genero,
			Date fechaInicial, Date fechaFinal) throws Exception {

		ListaEncadenada<VOPelicula> rta = new ListaEncadenada<>();
		MaxHeapCP<VOPelicula> aux = new MaxHeapCP<>();
		ArrayList<VOPelicula> ref = peliculasGenero.obtenerValor(genero.getNombre()).darMayores(fechaInicial);
		for (VOPelicula voPelicula : ref) {
			if(voPelicula.getFecha().compareTo(fechaFinal)<=0)
			{
				aux.agregar(voPelicula, comparator1());
			}
		}
		rta.agregarElementoFinal(aux.max(comparator1())); 
		return rta;
	}

	@Override
	public void agregarRatingConError(int idUsuario, int idPelicula,
			Double rating) {
		// TODO Auto-generated method stub
		VOUsuario user = userList.get(idUsuario);
		VOPelicula movie = peliculasId.get(idPelicula);
		double error = Math.abs(rating-calcularPrediccion(idUsuario,idPelicula));

		ListaEncadenada<VORating> ratingsUser = new ListaEncadenada<>();
		ListaEncadenada<VORelacionUsuarioPelicula> usersQueHanCalificado = new ListaEncadenada<>();
		ListaEncadenada<VORating> ratingsMovie = new ListaEncadenada<>();

		if(user.getRatings().isEmpty())
		{
			ratingsUser = user.getRatings();

			VORating nuevoRating = new VORating();
			nuevoRating.setError(error);
			nuevoRating.setMovieID(idPelicula);
			nuevoRating.setUserID(idUsuario);
			nuevoRating.setTimestamp(new Date().getTime());

			ratingsUser.agregarElementoFinal(nuevoRating);
			user.setRatings(ratingsUser);

			usersQueHanCalificado = movie.getUsuariosConRating();

			VORelacionUsuarioPelicula userPelicula = new VORelacionUsuarioPelicula(user.getIdUsuario(), rating);

			usersQueHanCalificado.agregarElementoFinal(userPelicula);
			movie.setUsuariosConRating(usersQueHanCalificado);

			ratingsMovie = movie.getRatingsAsociados();
			ratingsMovie.agregarElementoFinal(nuevoRating);
			movie.setRatingsAsociados(ratingsMovie);
		}
		else
		{
			VORating nuevoRating = new VORating();
			nuevoRating.setError(error);
			nuevoRating.setMovieID(idPelicula);
			nuevoRating.setUserID(idUsuario);
			nuevoRating.setTimestamp(new Date().getTime());

			ratingsUser.agregarElementoFinal(nuevoRating);
			user.setRatings(ratingsUser);

			usersQueHanCalificado = movie.getUsuariosConRating();

			VORelacionUsuarioPelicula userPelicula = new VORelacionUsuarioPelicula(user.getIdUsuario(), rating);

			usersQueHanCalificado.agregarElementoFinal(userPelicula);
			movie.setUsuariosConRating(usersQueHanCalificado);

			ratingsMovie = movie.getRatingsAsociados();
			ratingsMovie.agregarElementoFinal(nuevoRating);
			movie.setRatingsAsociados(ratingsMovie);
		}
	}

	@Override
	public ListaEncadenada<VOUsuarioPelicula> informacionInteraccionUsuario(int idUsuario) {
		ListaEncadenada<VOUsuarioPelicula> rta = new ListaEncadenada<VOUsuarioPelicula>();
		VOUsuario objetivo = usuariosId.get(idUsuario);
		ListaEncadenada<VORating> listarating = objetivo.getRatings();
		for (VORating voRating : listarating) {
			VOUsuarioPelicula nuevo = new VOUsuarioPelicula();
			double prediccion = calcularPrediccion(idUsuario, voRating.getMovieID());
			nuevo.setIdUsuario(idUsuario);
			nuevo.setNombrepelicula(peliculasId.get(voRating.getMovieID()).getNombre());
			nuevo.setRatingSistema(prediccion);
			nuevo.setRatingUsuario(voRating.getRating());
			nuevo.setTags((ILista<VOTag>) objetivo.getTags());
			double error = Math.abs((prediccion-voRating.getRating()));
			nuevo.setErrorRating(error);
			rta.agregarElementoFinal(nuevo);
		}

		return rta;
	}

	@Override
	public void clasificarUsuariosPorSegmento() {
		// TODO Auto-generated method stub
		if(diccionario.isEmpty())
			cargarDiccionario("./data/categoriestagsv2.csv");

		ArrayList<VOUsuario> users = userList.inOrden();
		int tagsConformes = 0;
		int tagsInconformes = 0;
		int tagsNeutrales = 0;
		int tagsNoClasificados = 0;

		for(VOUsuario usuario : users)
		{
			if(!usuario.getTags().isEmpty())
			{
				for(VOTag tag : usuario.getTags())
				{
					for(VOClasificacion clasif : diccionario)
					{
						if(tag.getTag().equals(clasif.getTag()))
						{
							if(clasif.getClasificacion().compareToIgnoreCase("conforme")==0)
								tagsConformes++;
							else if(clasif.getClasificacion().compareToIgnoreCase("inconforme")==0)
								tagsInconformes++;
							else if(clasif.getClasificacion().compareToIgnoreCase("neutral")==0)
								tagsNeutrales++;
							else
								tagsNoClasificados++;
						}
					}
				}
			}
			if(tagsConformes > tagsInconformes && tagsConformes > tagsNeutrales && tagsConformes > tagsNoClasificados)
				usuariosConformes.put(usuario.getIdUsuario(), usuario);
			else if(tagsInconformes > tagsConformes && tagsInconformes > tagsNeutrales && tagsInconformes > tagsNoClasificados)
				usuariosInconformes.put(usuario.getIdUsuario(), usuario);
			else if(tagsNeutrales > tagsConformes && tagsNeutrales > tagsInconformes && tagsNeutrales > tagsNoClasificados)
				usuariosNeutrales.put(usuario.getIdUsuario(), usuario);
			else
				usuariosNoClasificados.put(usuario.getIdUsuario(), usuario);
		}
	}

	@Override
	public void ordenarPeliculasPorAnho() {

		ArrayList<VOPelicula> array = peliculasId.inOrden();
		for (int i = 0; i < array.size(); i++) {

			VOPelicula objeto = array.get(i);
			int a�o = objeto.getA�o();
			ListaEncadenada<VOGeneroPelicula> listaGeneros = objeto.getGenerosAsociados();
			double ratingIMBD = objeto.getRatingIMBD();

			if(principal.contains(a�o))
			{
				for (VOGeneroPelicula list : listaGeneros) {
					if(!principal.get(a�o).existenValores(list.getNombre()))
					{
						RedBlackBST nuevoArbolSegundolvl = new RedBlackBST<Integer, ListaEncadenada<VOpeliculasUsuarios>>();
						principal.get(a�o).agregarElemento(list.getNombre(), nuevoArbolSegundolvl);
					}
					else
					{
						if(!principal.get(a�o).obtenerValor(list.getNombre()).contains(ratingIMBD))
						{
							ListaEncadenada<VOpeliculasUsuarios> lista3nivel= new ListaEncadenada<>();
							TablaHash<Integer, VOTag>objetosLista3 = darTagASociadosAPelicula(objeto);
							VOpeliculasUsuarios nuevaa = new VOpeliculasUsuarios(objeto, objetosLista3);//falta poner la tabla hash
							lista3nivel.agregarElementoFinal(nuevaa);
						}
						else
						{
							principal.get(a�o).obtenerValor(list.getNombre()).get(ratingIMBD).agregarElementoFinal(new VOpeliculasUsuarios(objeto, darTagASociadosAPelicula(objeto)));
						}
					}
				}
			}
			else
			{
				TablaHash<Integer, VOTag> hashUlitmolvl= darTagASociadosAPelicula(objeto);
				VOpeliculasUsuarios ultimolvl = new VOpeliculasUsuarios(objeto, hashUlitmolvl);//la tabla
				ListaEncadenada<VOpeliculasUsuarios> nuevolvl3 = new ListaEncadenada<>();
				nuevolvl3.agregarElementoFinal(ultimolvl);
				RedBlackBST<Double, ListaEncadenada<VOpeliculasUsuarios>> nuevolvl2 = new RedBlackBST<>();
				nuevolvl2.put(ratingIMBD, nuevolvl3);
				TablaHash1nodo<String, RedBlackBST<Double, ListaEncadenada<VOpeliculasUsuarios>>> nuevoNivel1 = new TablaHash1nodo<>();
				principal.put(a�o, nuevoNivel1);

			}
		}
	}

	@Override
	public VOReporteSegmento generarReporteSegmento(String segmento) {
		// TODO Auto-generated method stub
		double errorPromedio = 0.0;
		double ratingsConError = 0.0;
		double sumaErrorRatings = 0.0;
		
		ListaEncadenada<VOGeneroPelicula> generosMasRat = new ListaEncadenada<>();
		ListaEncadenada<VOGeneroPelicula> generosMejorPromedioRat = new ListaEncadenada<>();
		
		ListaEncadenadaLlaveValor<Integer,String> generosPeliculas = new ListaEncadenadaLlaveValor<>();
		ListaEncadenadaLlaveValor<Double, VOGeneroPelicula> generosPromedios = new ListaEncadenadaLlaveValor<>();
		
		double promedioRatingsGenero = 0.0;
		double sumaRatingsGenero = 0.0;
		double totalRatingsGenero = 0.0;

		if(segmento.compareToIgnoreCase("conforme")==0)
		{
			ArrayList<VOUsuario> usuariosConformesList = usuariosConformes.inOrden();
			int ratingsGenero = 0;
			for(int i = 0; i < usuariosConformesList.size(); i++)
			{
				VOUsuario user = (VOUsuario)usuariosConformesList.get(i);
				
				for(VORating rating : user.getRatings())
				{
					if(rating.getError()!=0.0)
					{
						sumaErrorRatings +=rating.getError();
						ratingsConError++;
					}
					VOPelicula pelicula = peliculasId.get(rating.getMovieID());
					for(int j = 0; j < pelicula.getGenerosAsociados().darNumeroElementos(); j++)
					{
						String genero = pelicula.getGenerosAsociados().darElemento(j).getNombre();
						if(!generosPeliculas.contains(genero))
						{
							generosPeliculas.agregarElementoFinal(0,genero);
							VOGeneroPelicula generoPelicula = new VOGeneroPelicula(genero);
							generosPromedios.agregarElementoFinal(0.0, generoPelicula);
						}
						else
						{
							RedBlackBST<Date, VOPelicula> temp = peliculasGenero.obtenerValor(genero);
							ArrayList<VOPelicula> peliculasGenero = temp.inOrden();
							for(int k = 0; k < peliculasGenero.size(); k++)
							{
								VOPelicula peliculaGenero = (VOPelicula)peliculasGenero.get(i);
								ratingsGenero += peliculaGenero.getRatingsAsociados().darNumeroElementos();
								for(VORating ratingGenero : peliculaGenero.getRatingsAsociados())
								{
									sumaRatingsGenero += ratingGenero.getRating();
									totalRatingsGenero++;
								}
							}
							promedioRatingsGenero = sumaRatingsGenero/totalRatingsGenero;
							VOGeneroPelicula generoPelicula = new VOGeneroPelicula(genero);
							generosPromedios.agregarElementoFinal(promedioRatingsGenero, generoPelicula);
							generosPeliculas.agregarElementoFinal(ratingsGenero, genero);
						}
					}
				}
			}
			Integer[] ratingsArray = new Integer[generosPeliculas.darNumeroElementos()];
			Iterator<Integer> iter = generosPeliculas.iterator();
			for(int i = 0; iter.hasNext(); i++)
			{
				Integer actual = iter.next();
				ratingsArray[i] = actual;
			}
			seleccion(ratingsArray);
			
			for(int i = 0; i < ratingsArray.length && i < 5; i++)
			{
				VOGeneroPelicula genero = new VOGeneroPelicula(generosPeliculas.darElemento(ratingsArray[i]));
				generosMasRat.agregarElementoFinal(genero);
			}
			
			Double[] promediosArray = new Double[generosPromedios.darNumeroElementos()];
			Iterator<Integer> iterator = generosPeliculas.iterator();
			for(int i = 0; iterator.hasNext(); i++)
			{
				Integer actual = iterator.next();
				ratingsArray[i] = actual;
			}
			seleccion(promediosArray);
			
			for(int i = 0; i < promediosArray.length && i < 5; i++)
			{
				generosMasRat.agregarElementoFinal(generosPromedios.darElemento(promediosArray[i]));
			}
		}
		else if(segmento.compareToIgnoreCase("inconforme")==0)
		{
			ArrayList<VOUsuario> usuariosInconformesList = usuariosInconformes.inOrden();
			int ratingsGenero = 0;
			for(int i = 0; i < usuariosInconformesList.size(); i++)
			{
				VOUsuario user = (VOUsuario)usuariosInconformesList.get(i);
				
				for(VORating rating : user.getRatings())
				{
					if(rating.getError()!=0.0)
					{
						sumaErrorRatings +=rating.getError();
						ratingsConError++;
					}
					VOPelicula pelicula = peliculasId.get(rating.getMovieID());
					for(int j = 0; j < pelicula.getGenerosAsociados().darNumeroElementos(); j++)
					{
						String genero = pelicula.getGenerosAsociados().darElemento(j).getNombre();
						if(!generosPeliculas.contains(genero))
						{
							generosPeliculas.agregarElementoFinal(0,genero);
							VOGeneroPelicula generoPelicula = new VOGeneroPelicula(genero);
							generosPromedios.agregarElementoFinal(0.0, generoPelicula);
						}
						else
						{
							RedBlackBST<Date, VOPelicula> temp = peliculasGenero.obtenerValor(genero);
							ArrayList<VOPelicula> peliculasGenero = temp.inOrden();
							for(int k = 0; k < peliculasGenero.size(); k++)
							{
								VOPelicula peliculaGenero = (VOPelicula)peliculasGenero.get(i);
								ratingsGenero += peliculaGenero.getRatingsAsociados().darNumeroElementos();
								for(VORating ratingGenero : peliculaGenero.getRatingsAsociados())
								{
									sumaRatingsGenero += ratingGenero.getRating();
									totalRatingsGenero++;
								}
							}
							promedioRatingsGenero = sumaRatingsGenero/totalRatingsGenero;
							VOGeneroPelicula generoPelicula = new VOGeneroPelicula(genero);
							generosPromedios.agregarElementoFinal(promedioRatingsGenero, generoPelicula);
							generosPeliculas.agregarElementoFinal(ratingsGenero, genero);
						}
					}
				}
			}
			Integer[] ratingsArray = new Integer[generosPeliculas.darNumeroElementos()];
			Iterator<Integer> iter = generosPeliculas.iterator();
			for(int i = 0; iter.hasNext(); i++)
			{
				Integer actual = iter.next();
				ratingsArray[i] = actual;
			}
			seleccion(ratingsArray);
			
			for(int i = 0; i < ratingsArray.length && i < 5; i++)
			{
				VOGeneroPelicula genero = new VOGeneroPelicula(generosPeliculas.darElemento(ratingsArray[i]));
				generosMasRat.agregarElementoFinal(genero);
			}
			
			Double[] promediosArray = new Double[generosPromedios.darNumeroElementos()];
			Iterator<Integer> iterator = generosPeliculas.iterator();
			for(int i = 0; iterator.hasNext(); i++)
			{
				Integer actual = iterator.next();
				ratingsArray[i] = actual;
			}
			seleccion(promediosArray);
			
			for(int i = 0; i < promediosArray.length && i < 5; i++)
			{
				generosMasRat.agregarElementoFinal(generosPromedios.darElemento(promediosArray[i]));
			}
		}
		else if(segmento.compareToIgnoreCase("nuetral")==0)
		{
			ArrayList<VOUsuario> usuariosNeutralesList = usuariosNeutrales.inOrden();
			int ratingsGenero = 0;
			for(int i = 0; i < usuariosNeutralesList.size(); i++)
			{
				VOUsuario user = (VOUsuario)usuariosNeutralesList.get(i);
				
				for(VORating rating : user.getRatings())
				{
					if(rating.getError()!=0.0)
					{
						sumaErrorRatings +=rating.getError();
						ratingsConError++;
					}
					VOPelicula pelicula = peliculasId.get(rating.getMovieID());
					for(int j = 0; j < pelicula.getGenerosAsociados().darNumeroElementos(); j++)
					{
						String genero = pelicula.getGenerosAsociados().darElemento(j).getNombre();
						if(!generosPeliculas.contains(genero))
						{
							generosPeliculas.agregarElementoFinal(0,genero);
							VOGeneroPelicula generoPelicula = new VOGeneroPelicula(genero);
							generosPromedios.agregarElementoFinal(0.0, generoPelicula);
						}
						else
						{
							RedBlackBST<Date, VOPelicula> temp = peliculasGenero.obtenerValor(genero);
							ArrayList<VOPelicula> peliculasGenero = temp.inOrden();
							for(int k = 0; k < peliculasGenero.size(); k++)
							{
								VOPelicula peliculaGenero = (VOPelicula)peliculasGenero.get(i);
								ratingsGenero += peliculaGenero.getRatingsAsociados().darNumeroElementos();
								for(VORating ratingGenero : peliculaGenero.getRatingsAsociados())
								{
									sumaRatingsGenero += ratingGenero.getRating();
									totalRatingsGenero++;
								}
							}
							promedioRatingsGenero = sumaRatingsGenero/totalRatingsGenero;
							VOGeneroPelicula generoPelicula = new VOGeneroPelicula(genero);
							generosPromedios.agregarElementoFinal(promedioRatingsGenero, generoPelicula);
							generosPeliculas.agregarElementoFinal(ratingsGenero, genero);
						}
					}
				}
			}
			Integer[] ratingsArray = new Integer[generosPeliculas.darNumeroElementos()];
			Iterator<Integer> iter = generosPeliculas.iterator();
			for(int i = 0; iter.hasNext(); i++)
			{
				Integer actual = iter.next();
				ratingsArray[i] = actual;
			}
			seleccion(ratingsArray);
			
			for(int i = 0; i < ratingsArray.length && i < 5; i++)
			{
				VOGeneroPelicula genero = new VOGeneroPelicula(generosPeliculas.darElemento(ratingsArray[i]));
				generosMasRat.agregarElementoFinal(genero);
			}
			
			Double[] promediosArray = new Double[generosPromedios.darNumeroElementos()];
			Iterator<Integer> iterator = generosPeliculas.iterator();
			for(int i = 0; iterator.hasNext(); i++)
			{
				Integer actual = iterator.next();
				ratingsArray[i] = actual;
			}
			seleccion(promediosArray);
			
			for(int i = 0; i < promediosArray.length && i < 5; i++)
			{
				generosMasRat.agregarElementoFinal(generosPromedios.darElemento(promediosArray[i]));
			}
		}
		else
		{
			ArrayList<VOUsuario> usuariosNoClasificadosList = usuariosNoClasificados.inOrden();
			int ratingsGenero = 0;
			for(int i = 0; i < usuariosNoClasificadosList.size(); i++)
			{
				VOUsuario user = (VOUsuario)usuariosNoClasificadosList.get(i);
				
				for(VORating rating : user.getRatings())
				{
					if(rating.getError()!=0.0)
					{
						sumaErrorRatings +=rating.getError();
						ratingsConError++;
					}
					VOPelicula pelicula = peliculasId.get(rating.getMovieID());
					for(int j = 0; j < pelicula.getGenerosAsociados().darNumeroElementos(); j++)
					{
						String genero = pelicula.getGenerosAsociados().darElemento(j).getNombre();
						if(!generosPeliculas.contains(genero))
						{
							generosPeliculas.agregarElementoFinal(0,genero);
							VOGeneroPelicula generoPelicula = new VOGeneroPelicula(genero);
							generosPromedios.agregarElementoFinal(0.0, generoPelicula);
						}
						else
						{
							RedBlackBST<Date, VOPelicula> temp = peliculasGenero.obtenerValor(genero);
							ArrayList<VOPelicula> peliculasGenero = temp.inOrden();
							for(int k = 0; k < peliculasGenero.size(); k++)
							{
								VOPelicula peliculaGenero = (VOPelicula)peliculasGenero.get(i);
								ratingsGenero += peliculaGenero.getRatingsAsociados().darNumeroElementos();
								for(VORating ratingGenero : peliculaGenero.getRatingsAsociados())
								{
									sumaRatingsGenero += ratingGenero.getRating();
									totalRatingsGenero++;
								}
							}
							promedioRatingsGenero = sumaRatingsGenero/totalRatingsGenero;
							VOGeneroPelicula generoPelicula = new VOGeneroPelicula(genero);
							generosPromedios.agregarElementoFinal(promedioRatingsGenero, generoPelicula);
							generosPeliculas.agregarElementoFinal(ratingsGenero, genero);
						}
					}
				}
			}
			Integer[] ratingsArray = new Integer[generosPeliculas.darNumeroElementos()];
			Iterator<Integer> iter = generosPeliculas.iterator();
			for(int i = 0; iter.hasNext(); i++)
			{
				Integer actual = iter.next();
				ratingsArray[i] = actual;
			}
			seleccion(ratingsArray);
			
			for(int i = 0; i < ratingsArray.length && i < 5; i++)
			{
				VOGeneroPelicula genero = new VOGeneroPelicula(generosPeliculas.darElemento(ratingsArray[i]));
				generosMasRat.agregarElementoFinal(genero);
			}
			
			Double[] promediosArray = new Double[generosPromedios.darNumeroElementos()];
			Iterator<Integer> iterator = generosPeliculas.iterator();
			for(int i = 0; iterator.hasNext(); i++)
			{
				Integer actual = iterator.next();
				ratingsArray[i] = actual;
			}
			seleccion(promediosArray);
			
			for(int i = 0; i < promediosArray.length && i < 5; i++)
			{
				generosMasRat.agregarElementoFinal(generosPromedios.darElemento(promediosArray[i]));
			}
		}
		errorPromedio = sumaErrorRatings/ratingsConError;
		VOReporteSegmento reporteSegmento = new VOReporteSegmento();
		reporteSegmento.setErrorPromedio(errorPromedio);
		reporteSegmento.setGenerosMasRatings(generosMasRat);
		reporteSegmento.setGenerosMejorPromedio(generosMejorPromedioRat);
		
		return reporteSegmento;
	}

	@Override
	public ILista<VOPelicula> peliculasGeneroPorFechaLanzamiento(
			VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPelicula> peliculasMayorPrioridad(int n) {
		// TODO Auto-generated method stub
		MaxHeapCP<VOPelicula> respuesta = new MaxHeapCP<>();
		respuesta.crearCP(n);
		
		ArrayList<VOPelicula> listaPeliculas = peliculasId.inOrden();
		Integer[] promediosAnuales = new Integer[listaPeliculas.size()];
		for(int i = 0; i < listaPeliculas.size(); i++)
		{
			VOPelicula pelicula = (VOPelicula)listaPeliculas.get(i);
			promediosAnuales[i] = pelicula.getPromedioAnualVotos();
		}
		seleccion(promediosAnuales);
		for(int i = 0; i < promediosAnuales.length && i < n; i++)
		{
			Integer promedio = promediosAnuales[i];
			for(int j = 0; j < listaPeliculas.size(); j++)
			{
				VOPelicula pelicula = (VOPelicula)listaPeliculas.get(i);
				Integer promedioPelicula = pelicula.getPromedioAnualVotos();
				if(promedio == promedioPelicula)
				{
					try
					{
						respuesta.agregar(pelicula, comparator2( ));
					}
					catch(Exception e)
					{
						//
					}
				}
			}
		}		
		return respuesta;
	}

	@Override
	public ILista<VOPelicula> consultarPeliculasFiltros(Integer anho,
			String pais, VOGeneroPelicula genero) throws Exception {

		ListaEncadenada<VOPelicula> rta = new ListaEncadenada<>();
		MaxHeapCP<VOPelicula> aux = new MaxHeapCP<>();
		ArrayList<VOPelicula> ref = peliculasId.inOrden();
		if(anho!=null && pais ==null && genero == null)
		{
			for (VOPelicula voPelicula : ref) {
				if(voPelicula.getA�o()==anho)
				{
					aux.agregar(voPelicula, comparator1());
				}
			}
		}
		else if(anho!=null && pais !=null && genero == null)
		{
			for (VOPelicula voPelicula : ref) {
				if(voPelicula.getA�o()==anho && voPelicula.getPais().equals(pais))
				{
					aux.agregar(voPelicula, comparator1());
				}
			}
		}
		else if(anho!=null && pais !=null && genero != null)
		{
			for (VOPelicula voPelicula : ref) {
				if(voPelicula.getA�o()==anho && voPelicula.getPais().equals(pais) && voPelicula.contieneEsteGenero(genero.getNombre()))
				{
					aux.agregar(voPelicula, comparator1());
				}
			}
		}
		return (ILista<VOPelicula>) rta;
	}

	public VOPelicula buscarPelicula(Integer idPelicula)
	{
		if(peliculasId.isEmpty())
			return null;
		else
		{
			if(peliculasId.contains(idPelicula))
				return peliculasId.get(idPelicula);
			else
				return null;			
		}
	}		
	public void inicializarUsuarios()
	{
		if(!ratingList.isEmpty() && !tagsList.isEmpty())
		{
			for(VORating vor : ratingList)
			{
				ListaEncadenada<VORating> ratingTemp = new ListaEncadenada<>();				
				if(!userList.contains(vor.getUserID()))
				{					
					VOUsuario vou = new VOUsuario();
					vou.setIdUsuario(vor.getUserID());
					ratingTemp.agregarElementoFinal(vor);
					vou.setRatings(ratingTemp);
					userList.put(vou.getIdUsuario(), vou);
				}
				else
				{
					VOUsuario user = userList.get(vor.getUserID());
					if(!user.getRatings().isEmpty())
					{
						ratingTemp = user.getRatings();
						ratingTemp.agregarElementoFinal(vor);
						user.setRatings(ratingTemp);
					}
					else
					{
						ratingTemp.agregarElementoFinal(vor);
						user.setRatings(ratingTemp);
					}
				}
			}
			for(VOTag vot : tagsList)
			{
				String[] infoTag = vot.getContenido().split(",");
				int userId = Integer.parseInt(infoTag[0]);
				ListaEncadenada<VOTag> tagTemp = new ListaEncadenada<>();
				if(!userList.contains(userId))
				{
					VOUsuario vou = new VOUsuario();
					vou.setIdUsuario(userId);
					tagTemp.agregarElementoFinal(vot);
					vou.setTags(tagTemp);
					userList.put(vou.getIdUsuario(), vou);
				}
				else
				{
					VOUsuario user = userList.get(userId);
					if(!user.getTags().isEmpty())
					{
						tagTemp = user.getTags();
						tagTemp.agregarElementoFinal(vot);
						user.setTags(tagTemp);
					}
					else
					{
						tagTemp.agregarElementoFinal(vot);
						user.setTags(tagTemp);
					}
				}
			}
		}
	}
	private TablaHash<Integer, VOTag> darTagASociadosAPelicula(VOPelicula arg1)
	{
		TablaHash<Integer, VOTag> objetosLista3 = new TablaHash<>(10);
		ArrayList<VOUsuario> array1 = usuariosId.inOrden();
		for (VOUsuario voUsuario : array1) {
			ListaEncadenada<VOTag> listadeTags = voUsuario.getTags();
			for (VOTag voTag : listadeTags) {
				if(voTag.getMovieID()==arg1.getMovieID())
				{
					objetosLista3.put(voTag.getUserID(), voTag);
				}
			}
		}
		return objetosLista3;
	}
	public void cargarPeliculasGenero()
	{

		ArrayList<VOPelicula> ref = peliculasId.inOrden();
		for (VOPelicula voPelicula : ref) {
			ListaEncadenada<VOGeneroPelicula> listagen = voPelicula.getGenerosAsociados();
			for (VOGeneroPelicula voGeneroPelicula : listagen) {
				if(peliculasGenero.existenValores(voGeneroPelicula.getNombre()))
				{
					if(peliculasGenero.obtenerValor(voGeneroPelicula.getNombre()).isEmpty())
					{
						RedBlackBST<Date, VOPelicula> nuevoArbol = new RedBlackBST<>();
						nuevoArbol.put(voPelicula.getFecha(), voPelicula);
					}
					else
					{
						peliculasGenero.obtenerValor(voGeneroPelicula.getNombre()).put(voPelicula.getFecha(), voPelicula);
					}
				}
				else
				{
					RedBlackBST<Date, VOPelicula> nuevoArbol = new RedBlackBST<>();
					nuevoArbol.put(voPelicula.getFecha(), voPelicula);
					peliculasGenero.agregarElemento(voGeneroPelicula.getNombre(), nuevoArbol);
				}
			}
		}

	}

	private void cargarDiccionario(String ruta)
	{
		File archivo = new File(ruta);
		try
		{
			FileReader fr = new FileReader(archivo);
			BufferedReader br = new BufferedReader(fr);

			String linea = br.readLine();
			linea = br.readLine();
			while(linea != null)
			{
				String[] info = linea.split(",");

				VOClasificacion clasificacion = new VOClasificacion();
				clasificacion.setTag(info[0]);
				clasificacion.setClasificacion(info[1]);

				diccionario.agregarElementoFinal(clasificacion);
			}
		}
		catch(Exception e)
		{

		}
	}

	private double darRatingUsuarioPelicula(int userId, int peliculaId)
	{
		double rating = 0.0;
		if(!userList.isEmpty()){
			VOUsuario user = userList.get(userId);
			if(!user.getRatings().isEmpty())
			{
				ListaEncadenada<VORating> ratingsUser = user.getRatings();
				for(VORating vor : ratingsUser)
				{
					if(vor.getMovieID() == peliculaId)
						rating = vor.getRating();
				}
			}
		}
		return rating;
	}
	
	private void seleccion(Integer[] a)
	{
		for(int i = 0; i < a.length; i++)
		{
			Integer mayor = i;
			for(int j = i+1; j < a.length; j++)
			{
				Integer totalRatings2 = a[j];
				if(totalRatings2 > a[mayor])
					mayor = j;
				
				Integer temp = a[i];
				a[i] = a[mayor];
				a[mayor] = temp;
			}
		}
	}
	private void seleccion(Double[] a)
	{
		for(int i = 0; i < a.length; i++)
		{
			Integer mayor = i;
			for(int j = i+1; j < a.length; j++)
			{
				Double actual = a[j];
				if(actual > a[mayor])
					mayor = j;
				
				Double temp = a[i];
				a[i] = a[mayor];
				a[mayor] = temp;
			}
		}
	}
	public Comparator<VOPelicula> comparator1()
	{
		Comparator<VOPelicula> a = new Comparator<VOPelicula>() {
			@Override
			public int compare(VOPelicula arg0, VOPelicula arg1) {
				int respuesta =0;
				if(arg0.getFecha().compareTo(arg1.getFecha())==0)
				{
					ArrayList<String> pais0 = arg0.getPais();
					ArrayList<String> pais1 = arg1.getPais();
					for (int i = 0; i < pais0.size(); i++) {
						for (int j = 0; j < pais1.size(); j++) {
							if(pais0.get(i).compareTo(pais1.get(j))!=0)
							{
								respuesta = pais0.get(i).compareTo(pais1.get(i))*-1;
								break;
							}
						}
					}
					if(respuesta==0)
					{
						ListaEncadenada<VOGeneroPelicula> generos0 = arg0.getGenerosAsociados();
						ListaEncadenada<VOGeneroPelicula> generos1 = arg1.getGenerosAsociados();
						for (VOGeneroPelicula voGeneroPelicula : generos0) {
							for (VOGeneroPelicula voGeneroPelicula2 : generos1) {
								if (voGeneroPelicula.getNombre().compareTo(voGeneroPelicula2.getNombre())!=0)
								{
									respuesta = voGeneroPelicula.getNombre().compareTo(voGeneroPelicula2.getNombre())*-1;
									break;
								}
							}
						}
						if(respuesta==0)
						{
							if(arg0.getRatingIMBD()>arg1.getRatingIMBD())
							{
								respuesta =-1;
							}
							else if(arg0.getRatingIMBD()<arg1.getRatingIMBD())
							{
								respuesta =1;
							}
						}
					}					
				}
				else
				{
					respuesta= arg0.getFecha().compareTo(arg1.getFecha())*-1;
				}
				return respuesta;
			}
		};
		return a;
	}
	public Comparator<VOPelicula> comparator2()
	{
		return new Comparator<VOPelicula>(){

			@Override
			public int compare(VOPelicula arg0, VOPelicula arg1) {
				// TODO Auto-generated method stub
				int cmp = 0;
				
				if(arg0.getPromedioAnualVotos() < arg1.getPromedioAnualVotos())
					cmp = -1;
				
				else if(arg0.getPromedioAnualVotos() > arg1.getPromedioAnualVotos())
					cmp = 1;
			
				return cmp;
			}
			
		};
	}
}
