package API;

import API.ListaEncadenada;

public class Cola<T> implements IQueue<T> {

	/**
	 * Referencia a una lista doble encadenada encargada de manejar los elementos de la cola
	 */
	private ListaEncadenada<T> list;
	
	/**
	 * Crea una nueva cola vac�a
	 */
	public Cola()
	{
		list = new ListaEncadenada<T>();
	}
	
	/**
	 * Agrega un elemento a la �ltima posici�n de la cola
	 */
	@Override
	public void enqueue(T item) {
		// TODO Auto-generated method stub
		list.agregarElementoFinal(item);
	}

	/**
	 * Elimina el elemento en la primera posici�n de la cola
	 */
	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		return list.eliminarObjeto(0);
	}

	/**
	 * Indica si la cola est� vac�a
	 * @return true si la cola no tiene elementos
	 * @return false si la cola tiene uno o m�s elementos
	 */
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return list.darNumeroElementos() == 0;
	}

	/**
	 * Indica la cantidad de elementos que tiene la cola
	 * @return n�mero de elementos de la cola
	 */
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return list.darNumeroElementos();
	}

}
