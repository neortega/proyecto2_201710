package vos;


public class VORelacionUsuarioPelicula {
		
	private int idUsuario;
	
	private double rating;
	
	private double ratingPeliculaActual;
	
	private double ratingPeliculaAComparar;

	public VORelacionUsuarioPelicula( int idUsuario, double rating) {
		this.idUsuario = idUsuario;
		this.rating = rating;
		this.ratingPeliculaActual =0.0;
		this.ratingPeliculaAComparar= 0.0;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public double getRatingPeliculaActual() {
		return ratingPeliculaActual;
	}

	public void setRatingPeliculaActual(double ratingPeliculaActual) {
		this.ratingPeliculaActual = ratingPeliculaActual;
	}

	public double getRatingPeliculaAComparar() {
		return ratingPeliculaAComparar;
	}

	public void setRatingPeliculaAComparar(double ratingPeliculaAComparar) {
		this.ratingPeliculaAComparar = ratingPeliculaAComparar;
	}

	
	
	
}
