package vos;

import model.data_structures.TablaHash;

public class VOpeliculasUsuarios {

private VOPelicula pelicula;
	
	private TablaHash<Integer, VOTag> usuarios;

	public VOpeliculasUsuarios(VOPelicula pelicula,
			TablaHash<Integer, VOTag> usuarios) {
		super();
		this.pelicula = pelicula;
		this.usuarios = usuarios;
	}

	public VOPelicula getPelicula() {
		return pelicula;
	}

	public void setPelicula(VOPelicula pelicula) {
		this.pelicula = pelicula;
	}

	public TablaHash<Integer, VOTag> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(TablaHash<Integer, VOTag> usuarios) {
		this.usuarios = usuarios;
	}
}
