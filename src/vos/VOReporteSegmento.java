package vos;
import API.*;
/**
 * Esta clase modela el reporte que se debe generar por segmento
 * @author Venegas
 *
 */
public class VOReporteSegmento{


/*
	 *  el error promedio  (suma de errores sobre sus ratings dividido la cantidad de ratings con 
	 *error asociado)
	 */
	private double errorPromedio;
	
	/*
	 * 5 g�neros con m�s cantidad de ratings
	 * 
	 */
	
	private ListaEncadenada<VOGeneroPelicula> generosMasRatings;

	/*
	 *  sus 5 g�neros con mejor rating promedio.
	 */
	
	private ListaEncadenada<VOGeneroPelicula> generosMejorPromedio;
	
	
	public VOReporteSegmento() {
		// TODO Auto-generated constructor stub
	}


	public double getErrorPromedio() {
		return errorPromedio;
	}


	public void setErrorPromedio(double errorPromedio) {
		this.errorPromedio = errorPromedio;
	}


	public ListaEncadenada<VOGeneroPelicula> getGenerosMasRatings() {
		return generosMasRatings;
	}


	public void setGenerosMasRatings(ListaEncadenada<VOGeneroPelicula> generosMasRatings) {
		this.generosMasRatings = generosMasRatings;
	}


	public ListaEncadenada<VOGeneroPelicula> getGenerosMejorPromedio() {
		return generosMejorPromedio;
	}


	public void setGenerosMejorPromedio(ListaEncadenada<VOGeneroPelicula> generosMejorPromedio) {
		this.generosMejorPromedio = generosMejorPromedio;
	
	}
}
