package vos;

public class VOSimilitud {

	private int idPeliculaActual;
	
	private String nombrePeliculaActual;
	
	private int idPeliculaComparada;
	
	private String nombrePeliculaComparada;
	
	private double similitud;

	public VOSimilitud(int idPeliculaActual, String nombrePeliculaActual,
			int idPeliculaComparada, String nombrePeliculaComparada,
			double similitud) {
		super();
		this.idPeliculaActual = idPeliculaActual;
		this.nombrePeliculaActual = nombrePeliculaActual;
		this.idPeliculaComparada = idPeliculaComparada;
		this.nombrePeliculaComparada = nombrePeliculaComparada;
		this.similitud = similitud;
	}

	public int getIdPeliculaActual() {
		return idPeliculaActual;
	}

	public void setIdPeliculaActual(int idPeliculaActual) {
		this.idPeliculaActual = idPeliculaActual;
	}

	public String getNombrePeliculaActual() {
		return nombrePeliculaActual;
	}

	public void setNombrePeliculaActual(String nombrePeliculaActual) {
		this.nombrePeliculaActual = nombrePeliculaActual;
	}

	public int getIdPeliculaComparada() {
		return idPeliculaComparada;
	}

	public void setIdPeliculaComparada(int idPeliculaComparada) {
		this.idPeliculaComparada = idPeliculaComparada;
	}

	public String getNombrePeliculaComparada() {
		return nombrePeliculaComparada;
	}

	public void setNombrePeliculaComparada(String nombrePeliculaComparada) {
		this.nombrePeliculaComparada = nombrePeliculaComparada;
	}

	public double getSimilitud() {
		return similitud;
	}

	public void setSimilitud(double similitud) {
		this.similitud = similitud;
	}
	
	
	
	
}
