package vos;

public class VOTag {
	
	/**
	 * contenido del tag
	 */
	private String contenido;
	
	
	public VOTag() {
		
		// TODO Auto-generated constructor stub
	}
	//
	// El contenido del tag est� compuesto de la siguiente manera:
	// userID, movieID, tag, timestamp
	// 
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	public int getUserID() {
		String[] a = contenido.split(",");
		return (int)Integer.parseInt(a[0]);
	}
	public int getMovieID() {
		String[] a = contenido.split(",");
		return (int)Integer.parseInt(a[1]);
	}
	public String getTag() {
		String[] a = contenido.split(",");
		return a[2];
	}
	public Long getTimestamp() {
		String[] a = contenido.split(",");
		return (Long )Long.parseLong(a[3]);
	}
	

}
