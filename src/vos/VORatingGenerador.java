package vos;

public class VORatingGenerador {

	int item_id;
	
	double p_rating;

	public int getItemId() {
		return item_id;
	}

	public void setItemId(int item_id) {
		this.item_id = item_id;
	}

	public double getPRating() {
		return p_rating;
	}

	public void setPRating(double p_rating) {
		this.p_rating = p_rating;
	}
}
