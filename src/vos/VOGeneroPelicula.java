package vos;

public class VOGeneroPelicula {

	/*
	 * nombre del genero
	 */
	
	private String nombre;
		
	public VOGeneroPelicula(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
