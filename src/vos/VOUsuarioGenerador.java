package vos;
import API.*;

public class VOUsuarioGenerador {

	int user_id;
	
	ListaEncadenada<VORatingGenerador> recommendations;

	public int getUserId() {
		return user_id;
	}

	public void setUserId(int user_id) {
		this.user_id = user_id;
	}

	public ListaEncadenada<VORatingGenerador> getRecommendations() {
		return recommendations;
	}

	public void setRecommendations(
			ListaEncadenada<VORatingGenerador> recommendations) {
		this.recommendations = recommendations;
	}	
}
