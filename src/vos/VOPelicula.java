package vos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import API.ILista;
import API.ListaEncadenada;

public class VOPelicula {
	/*
	 * nombre de la pel�cula
	 */
	private String nombre;

	/*
	 * Fecha de lanzamiento de la pel�cula
	 */
	private int a�o;

	private int mes;

	private int dia;

	/*
	 * Lista con los generos asociados a la pel�cula
	 */
	private ListaEncadenada<VOGeneroPelicula> generosAsociados;
	
	private ListaEncadenada<VORating> ratingsAsociados;
	
	private ListaEncadenada<VOTag> tagsAsociados;
	
	private ListaEncadenada<VORelacionUsuarioPelicula> usuariosConRating;
	/*
	 * votos totales sobre la pel�cula
	 */
	private int votostotales;  

	/*
	 * promedio anual de votos (hasta el 2016)
	 */
	private int promedioAnualVotos; 


	private int movieID;
	/*
	 * promedio IMBD
	 */

	private double ratingIMBD;	
	
	private Date fecha;
	
	private ArrayList<String> pais;

	public VOPelicula(String nombre, int a�o, String mes, int dia,
			ListaEncadenada<VOGeneroPelicula> generosAsociados, int votostotales,
			int promedioAnualVotos, double ratingIMBD, int movieID, ArrayList<String> pais) throws ParseException {

		this.nombre = nombre;
		this.a�o =a�o;
		this.mes = calcularMes(mes);
		this.dia = dia;
		this.generosAsociados = generosAsociados;
		this.votostotales = votostotales;
		this.promedioAnualVotos = promedioAnualVotos;
		this.ratingIMBD = ratingIMBD;
		this.movieID = movieID;
		usuariosConRating = new ListaEncadenada<>();
		this.pais = pais;
		calcularFecha();
	}

	private void calcularFecha() throws ParseException {
		String a = "" + dia + "-" + mes + "-" + a�o;
		SimpleDateFormat st = new SimpleDateFormat( "dd-MM-YYYY");
		fecha = st.parse(a);	
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getA�o() {
		return a�o;
	}

	public void setA�o(int a�o) {
		this.a�o = a�o;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}

	public ListaEncadenada<VOGeneroPelicula> getGenerosAsociados() {
		return generosAsociados;
	}

	public void setGenerosAsociados(ListaEncadenada<VOGeneroPelicula> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}

	public int getVotostotales() {
		return votostotales;
	}

	public void setVotostotales(int votostotales) {
		this.votostotales = votostotales;
	}

	public int getPromedioAnualVotos() {
		return promedioAnualVotos;
	}

	public void setPromedioAnualVotos(int promedioAnualVotos) {
		this.promedioAnualVotos = promedioAnualVotos;
	}

	public double getRatingIMBD() {
		return ratingIMBD;
	}

	public void setRatingIMBD(double ratingIMBD) {
		this.ratingIMBD = ratingIMBD;
	}

	public int getMovieID() {
		return movieID;
	}

	public void setMovieID(int movieID) {
		this.movieID = movieID;
	}
	public ListaEncadenada<VORating> getRatingsAsociados() {
		return ratingsAsociados;
	}

	public void setRatingsAsociados(ListaEncadenada<VORating> ratingsAsociados) {
		this.ratingsAsociados = ratingsAsociados;
	}

	public ListaEncadenada<VOTag> getTagsAsociados() {
		return tagsAsociados;
	}

	public void setTagsAsociados(ListaEncadenada<VOTag> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	
	public ListaEncadenada<VORelacionUsuarioPelicula> getUsuariosConRating() {
		return usuariosConRating;
	}

	public void setUsuariosConRating(
			ListaEncadenada<VORelacionUsuarioPelicula> usuariosConRating) {
		this.usuariosConRating = usuariosConRating;
	}
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	

	public ArrayList<String> getPais() {
		return pais;
	}

	public void setPais(ArrayList<String> pais) {
		this.pais = pais;
	}

	public boolean contieneEsteGenero(String genero)
	{
		boolean rta = false;
		for (VOGeneroPelicula element : generosAsociados) {
			if(genero.equals(element.getNombre()))
			{
				rta = true;
			}
		}
		return rta;
	}
	private int calcularMes(String mes)
	{
		int respuesta =0;
		if(mes.compareToIgnoreCase("Jan")==0)
			respuesta=1;
		else if(mes.compareToIgnoreCase("Feb")==0)
			respuesta=2;
		else if(mes.compareToIgnoreCase("Mar")==0)
			respuesta=3;
		else if(mes.compareToIgnoreCase("Apr")==0)
			respuesta=4;
		else if(mes.compareToIgnoreCase("May")==0)
			respuesta=5;
		else if(mes.compareToIgnoreCase("Jun")==0)
			respuesta=6;
		else if(mes.compareToIgnoreCase("Jul")==0)
			respuesta=7;
		else if(mes.compareToIgnoreCase("Aug")==0)
			respuesta=8;
		else if(mes.compareToIgnoreCase("Sep")==0)
			respuesta=9;
		else if(mes.compareToIgnoreCase("Oct")==0)
			respuesta=10;
		else if(mes.compareToIgnoreCase("Nov")==0)
			respuesta=11;
		else if(mes.compareToIgnoreCase("Dec")==0)
			respuesta=12;
		return respuesta;
	}



}
