package vos;
import API.*;

public class VOUsuario {
	
	private String nombre;
	
	private int idUsuario;
	
	ListaEncadenada<VOTag> tagList;
	
	ListaEncadenada<VORating> ratingList;

	public VOUsuario()
	{
		tagList = new ListaEncadenada<>();
		ratingList = new ListaEncadenada<>();
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public ListaEncadenada<VOTag> getTags() {
		return tagList;
	}

	public void setTags(ListaEncadenada<VOTag> tagList) {
		this.tagList = tagList;
	}

	public ListaEncadenada<VORating> getRatings() {
		return ratingList;
	}

	public void setRatings(ListaEncadenada<VORating> ratingList) {
		this.ratingList = ratingList;
	}
}
