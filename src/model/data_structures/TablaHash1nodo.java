package model.data_structures;

public class TablaHash1nodo<K  extends String,V> {

	private V[] array;	
	private int numeroElementos;

	public TablaHash1nodo ()
	{
		array = (V[])new Object[19];
	}
	public int darNumeroElementos()
	{
		return numeroElementos;
	}
	public void agregarElemento (K llave,V valor)
	{
		numeroElementos++;
		int posicion = hash(llave);
		array[posicion]=valor;
	}	
	
	public V obtenerValor (K llave)
	{
		return (V) array[hash(llave)];
	}
	public boolean existenValores(K llave) 
	{
		return  (V) array[hash(llave)]==null;
	}
	public int hash(K llave) {

		int respuesta =0;
		
		if(llave.compareToIgnoreCase("Action")==0)
			respuesta =1;
		else if(llave.compareToIgnoreCase("Adventure")==0)
			respuesta =2;
		else if(llave.compareToIgnoreCase("Animation")==0)
			respuesta =3;
		else if(llave.compareToIgnoreCase("Children's")==0)
			respuesta =4;
		else if(llave.compareToIgnoreCase("Comedy")==0)
			respuesta =5;
		else if(llave.compareToIgnoreCase("Crime")==0)
			respuesta =6;
		else if(llave.compareToIgnoreCase("Documentary")==0)
			respuesta =7;
		else if(llave.compareToIgnoreCase("Drama")==0)
			respuesta =8;
		else if(llave.compareToIgnoreCase("Fantasy")==0)
			respuesta =9;
		else if(llave.compareToIgnoreCase("Film-Noir")==0)
			respuesta =10;
		else if(llave.compareToIgnoreCase("Horror")==0)
			respuesta =11;
		else if(llave.compareToIgnoreCase("Musical")==0)
			respuesta =12;
		else if(llave.compareToIgnoreCase("Mystery")==0)
			respuesta =13;
		else if(llave.compareToIgnoreCase("Romance")==0)
			respuesta =14;
		else if(llave.compareToIgnoreCase("Sci-Fi")==0)
			respuesta =15;
		else if(llave.compareToIgnoreCase("Thriller")==0)
			respuesta =16;
		else if(llave.compareToIgnoreCase("War")==0)
			respuesta =17;
		else if(llave.compareToIgnoreCase("Western")==0)
			respuesta =18;

		return respuesta;
	}
	

}
