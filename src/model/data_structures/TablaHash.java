package model.data_structures;

public class TablaHash<Key, Value> {
	private static final int CAPACIDAD = 4;
	private int numeroElementos;                                // number of key-value pairs
    private int tama�o;                                // hash table size
    private Value[] arreglo;  // array of linked-list symbol tables


      /**
     * Initializes an empty symbol table with {@code m} chains.
     * @param tama�o the initial number of chains
     */
    public TablaHash(int m) {
        this.tama�o = m;
        arreglo = (Value[]) new Object[m];
    } 

    // resize the hash table to have the given number of chains,
    // rehashing all of the keys
    private void resize(int chains) {
    	 Value[] temp = (Value[]) new Object[chains];
        for (int i = 0; i < tama�o; i++) {
            temp[i] = arreglo[i]; 
        }
        this.tama�o  = temp.length;
        this.arreglo = temp;
    }

    // hash value between 0 and m-1
    private int hash(Key key) {
        return (key.hashCode() & 0x7fffffff) % tama�o;
    } 

    /**
     * Returns the number of key-value pairs in this symbol table.
     *
     * @return the number of key-value pairs in this symbol table
     */
    public int size() {
        return numeroElementos;
    } 

    /**
     * Returns true if this symbol table is empty.
     *
     * @return {@code true} if this symbol table is empty;
     *         {@code false} otherwise
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Returns true if this symbol table contains the specified key.
     *
     * @param  key the key
     * @return {@code true} if this symbol table contains {@code key};
     *         {@code false} otherwise
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public boolean contains(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to contains() is null");
        return get(key) != null;
    } 

    /**
     * Returns the value associated with the specified key in this symbol table.
     *
     * @param  key the key
     * @return the value associated with {@code key} in the symbol table;
     *         {@code null} if no such value
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public Value get(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to get() is null");
        int i = hash(key);
        return arreglo[i];
    } 

    /**
     * Inserts the specified key-value pair into the symbol table, overwriting the old 
     * value with the new value if the symbol table already contains the specified key.
     * Deletes the specified key (and its associated value) from this symbol table
     * if the specified value is {@code null}.
     *
     * @param  key the key
     * @param  val the value
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public void put(Key key, Value val) {
        if (key == null) throw new IllegalArgumentException("first argument to put() is null");
        if (val == null) {
            delete(key);
            return;
        }

        // double table size if average length of list >= 10
        if (numeroElementos >= 10*tama�o) resize(2*tama�o);

        int i = hash(key);
        numeroElementos++;
        arreglo[i]=val;
    } 

    /**
     * Removes the specified key and its associated value from this symbol table     
     * (if the key is in this symbol table).    
     *
     * @param  key the key
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public void delete(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to delete() is null");

        int i = hash(key);
        if (arreglo[i]==null) numeroElementos--;
        arreglo[i]=null;

        // halve table size if average length of list <= 2
        if (tama�o > CAPACIDAD && numeroElementos <= 2*tama�o) resize(tama�o/2);
    } }