package model.data_structures;

public class NodoSencillo <K,T> {
	
	private T objeto;
	
	private K llave;
	
	private NodoSencillo<K,T> siguiente;
	
	/**
	 * Crea un nuevo nodo vac�o
	 */
	public NodoSencillo(K pKey,T pObjeto)
	{
		llave=pKey;
		siguiente = null;
		objeto = pObjeto;
	}
	public K darLlave()
	{
		return llave;
	}
	public void modificarObjeto(T pObjeto)
	{
		objeto = pObjeto;
	}
	
	public T darObjeto()
	{
		return objeto;
	}
	
	public NodoSencillo<K,T> darSiguiente()
	{
		return siguiente;
	}
	
	public void modificarSiguiente(NodoSencillo<K,T> pNodo)
	{
		siguiente = pNodo;
	}
}
