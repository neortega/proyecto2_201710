package model.data_structures;

import java.util.Comparator;

public class HeapSort<T> {

	private T[] array;

	private int length;
	
	
	public T[] heapSort (Comparator<T> a , T[] arreglo)
	{
		array =arreglo;
		length = array.length;
		for (int i = length/2; i > 0; i--) {
			sink(i, a, length);
		}
		for (int i = 0; i < length; i++) {
			T objetoMayor = array[1];
			T objetoMenor = array[length];
			array[1] = objetoMenor;
			array[length] = objetoMayor;
			length--;
			sink(1 ,a,length);
		}
		return array;
	}
	private void sink(int k, Comparator<T> a , int n)
	{
		while(2*k <= n)
		{
			int j = 2*k;
			if(j< n && a.compare(array[j], array[j+1])<0) j++;
			if (a.compare(array[j], array[j+1])>0) break;
			T elem = array[k];
			T elem2 = array[j];
			array[k] = elem2;
			array[j] = elem;
			k=j;
		}
	}
	
}
