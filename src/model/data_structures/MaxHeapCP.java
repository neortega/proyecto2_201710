package model.data_structures;

import java.util.Comparator;

public class MaxHeapCP <T> implements API.ILista<T>  {


	private T[] array;

	private int length;
	
	public void crearCP(int max) {
		
		array = (T[])new Object[max+1];
		length = 0;
	}

	
	public int darNumElementos() {
		return length;
	}

	public void agregar(T elemento, Comparator<T> a) throws Exception {
		length++;
		array[length] = elemento;
		swim(length,a);
	}

	public boolean esVacia() {
		return length==0;
	}

	public int tamanoMax() {
		
		return array.length;
	}
	private void swim (int k , Comparator<T> a)
	{
		while (k> 1 && a.compare(array[k/2], array[k])<0)
		{
			T elem = array[k];
			T elem2 = array[k/2];
			array[k] = elem2;
			array[k/2] = elem;
			k = k/2;
		}
	}
	private void sink(int k, Comparator<T> a)
	{
		while(2*k <= length)
		{
			
			int j = 2*k;
			if(j< length && a.compare(array[j], (array[j+1])) <0 ) j++;
			if (a.compare(array[j], (array[j+1]))>0) break;
			T elem = array[k];
			T elem2 = array[j];
			array[k] = elem2;
			array[j] = elem;
			k=j;
		
		}
	}

	public T max( Comparator<T> a) {
		if(length==0)return null;
		else
		{
			T elem = array[length];
			T elemBuscado = array[1];
			array[length] = elemBuscado;
			array[1] = elem;
			length--;
			sink(1,a);
			return elemBuscado;
		}
	}
	
	public T darElemento(int pos)
	{
		if(esVacia())
			return null;
		
		T obj = null;
		for(int i = 0; i < array.length; i++)
		{
			if(pos == i)
				obj = array[i];
		}
		return obj;
	}
}
